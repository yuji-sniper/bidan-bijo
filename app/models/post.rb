class Post < ApplicationRecord
    has_many :comments, dependent: :destroy
    
    validates :image, {presence: true}
    validates :name, {presence: true}
    validates :gender, {presence: true}
    validates :country, {presence: true}
    validates :job, {presence: true}
    validates :face, {presence: true}
    validates :content, {presence: true}
    validates :birthday, length: { is: 8 }
    
    def self.search(search)
        if search
          Post.where('name LIKE? OR gender LIKE? OR country LIKE? OR job LIKE? OR content LIKE?',
                    "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%")
        else
          Post.all
        end
    end    
  
end
