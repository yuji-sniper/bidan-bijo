class PostsController < ApplicationController
  def index
    @posts = Post.order(created_at: :desc).paginate(page: params[:page], per_page: 8)
  end
  
  def new
    @post = Post.new
  end
  
  def create
    @post = Post.new(image: params[:image],
                     name: params[:name], 
                     gender: params[:gender], 
                     country: params[:country], 
                     job: params[:job], 
                     face: params[:face], 
                     content: params[:content], 
                     view: 0,
                     birthday: params[:year] + params[:month] + params[:day],
                     )
    if @post.save
      @post.image = "#{@post.id}.jpg"
      @post.save
      image = params[:image]
      File.binwrite("public/images/#{@post.image}", image.read)
      redirect_to ("/posts/index")
    else
      render("posts/new")
    end  
  end 
  
  def edit
    @post = Post.find_by(id: params[:id])
  end
  
  def update
    @post = Post.find_by(id: params[:id])
    @post.name = params[:name]
    @post.gender = params[:gender]
    @post.country = params[:country]
    @post.job = params[:job]
    @post.face = params[:face]
    @post.content = params[:content]
    @post.birthday = params[:year] + params[:month] + params[:day]
    if params[:image]
      @post.image = "#{@post.id}.jpg"
      image = params[:image]
      File.binwrite("public/images/#{@post.image}", image.read)
    else
      @post.image = "#{@post.id}.jpg"
    end
    if @post.save
      redirect_to("/posts/#{@post.id}")
    else
      render("posts/edit")
    end  
  end
  
  def destroy
    @post = Post.find_by(id: params[:id])
    @post.destroy
    File.delete("public/images/#{@post.image}")
    redirect_to("/posts/index")
  end  
  
  def show
    @post = Post.find_by(id: params[:id])
    @post.view += 1
    @post.save
    @age = (Date.today.strftime("%Y%m%d").to_i - @post.birthday.to_i) / 10000
  end
  
  def search
    @posts = Post.search(params[:search])
  end
  
  def ranking
    # @posts = Post.all.order(view: :desc)
    @posts = Post.order(view: :desc).paginate(page: params[:page], per_page: 8)
  end  
  
  
  def male
    @posts = Post.where(gender: "男性")
  end
  
  def female
    @posts = Post.where(gender: "女性")
  end
  
  def america
    @posts = Post.where(country: "アメリカ")
  end
  
  def england
    @posts = Post.where(country: "イギリス")
  end
  
  def canada
    @posts = Post.where(country: "カナダ")
  end
  
  def japan
    @posts = Post.where(country: "日本")
  end
  
  def danmark
    @posts = Post.where(country: "デンマーク")
  end
  
  
  
  def actor
    @posts = Post.where(job: "俳優")
  end
  
  def actress
    @posts = Post.where(job: "女優")
  end
  
  def musician
    @posts = Post.where(job: "ミュージシャン")
  end
  
  def sports
    @posts = Post.where(job: "スポーツ選手")
  end
  
  
  
    
  
end
