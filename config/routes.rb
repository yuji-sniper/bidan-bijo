Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  # get "posts/index" => "posts#index"
  root "posts#index"
  get "posts/new" => "posts#new"
  post "posts/create" => "posts#create"
  get "posts/search" => "posts#search"
  get "posts/ranking" => "posts#ranking"

  get "posts/male" => "posts#male"
  get "posts/female" => "posts#female"
  
  get "posts/america" => "posts#america"
  get "posts/england" => "posts#england"
  get "posts/canada" => "posts#canada"
  get "posts/japan" => "posts#japan"
  get "posts/danmark" => "posts#danmark"
  
  get "posts/actor" => "posts#actor"
  get "posts/actress" => "posts#actress"
  get "posts/musician" => "posts#musician"
  get "posts/sports" => "posts#sports"

  get "posts/:id" => "posts#show"
  get "posts/:id/edit" => "posts#edit"
  post "posts/:id/update" => "posts#update"
  post "posts/:id/destroy" => "posts#destroy"

  
  resources :posts do
    resources :comments
  end  
  
end
