class AddBirthdayToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :birthday, :string
  end
end
