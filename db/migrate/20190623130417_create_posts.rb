class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :name
      t.string :image
      t.string :gender
      t.string :country
      t.string :job
      t.string :face
      t.text :content

      t.timestamps
    end
  end
end
